module agent

go 1.15

require (
	github.com/danieljoos/wincred v1.1.2
	github.com/eknkc/amber v0.0.0-20171010120322-cdade1c07385 // indirect
	github.com/gin-contrib/timeout v0.0.1 // indirect
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kardianos/service v1.2.0
	github.com/karrick/godirwalk v1.16.1
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mitchellh/go-ps v1.0.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pjebs/jsonerror v0.0.0-20190614034432-63ef9a8df848 // indirect
	github.com/pjebs/restgate v0.0.0-20200504001537-fd9a58a4fe75
	github.com/rs/xid v1.3.0
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.7.0
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/sys v0.0.0-20211102061401-a2f17f7b995c // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/unrolled/render.v1 v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
